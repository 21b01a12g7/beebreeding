hexa_coad= [[1, 0, -1], [0, 1, -1], [-1, 1, 0], [-1, 0, 1], [0, -1, 1], [1, -1, 0]]
RADIUS = 58
EDGES=6
def createGrid(radius):
    hexa_num = 1
    hexaDict = {}
    p_coad, q_coad, r_coad = 0, 0, 0
    hexaDict[hexa_num] = p_coad, q_coad, r_coad
    for rad in range(radius):
        p_coad = 0
        q_coad = -rad
        r_coad = rad
        for i in range(6):
            for j in range(rad):
                p_coad += hexa_coad[i][0]
                q_coad += hexa_coad[i][1]
                r_coad += hexa_coad[i][2]
                hexa_num += 1
                hexaDict[hexa_num] = p_coad,q_coad, r_coad

    return hexaDict

while(True):
    hexa_num1, hexa_num2 = map(int, input().split())
    if hexa_num1 == 0 and hexa_num2 == 0:
        break
    else:
        print(min_distance(createGrid(RADIUS), hexa_num1, hexa_num2))
